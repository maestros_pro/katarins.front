onmessage = function(e){

	var data = e.data;

	setInterval(function(){
		var xhr;
		var json = JSON.stringify(data);
		var param = Object.keys(data).map(function(k) {
			return encodeURIComponent(k) + '=' + encodeURIComponent(data[k])
		}).join('&');

		xhr = new XMLHttpRequest();
		xhr.open('POST', '/lk/check_new_messages', false);
		xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
		xhr.setRequestHeader('Cache-Control', 'post-check=0,pre-check=0, false');
		xhr.setRequestHeader('Cache-Control',  'max-age=0, false');
		xhr.setRequestHeader('Pragma', 'no-cache');
		xhr.setRequestHeader('Cache-Control', 'no-cache, must-revalidate');
		xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");
		xhr.send(param);
		if (xhr.status == 200 ){
			var res = JSON.parse(xhr.response);
			if ( res.messages.length ){
				data.last_message_id = res.last_message_id;
				postMessage(res);
			}
		}
	}, 2000);
};
