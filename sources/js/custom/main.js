

var appasyncadd = appasyncadd || [];
(function(){
	var app = {
		module: {

			templateItem:{
				current: {
					id: '',
					src: '',
					data: {
						img: '',
						preview: '',
						measure: {
							top: '',
							left: '',
							width: '',
							height: ''
						}
					}
				},
				storage:{},

				showImage: function(_name, _size, _width, _height){

					var name = _name ? ('Название файла - ' + _name + '<br>') : '';
					var size = _size ? ('Вес файла - ' + (_size/1000).toFixed(2) + ' kB<br>') : '';
					var dim = (_width && _height) ? ('Размеры исходного изображения: ширина - ' + _width + ' px, высота ' + _height + ' px.') : '';

					var html = '<div class="popup-cropper ' + self.templateItem.current.ft + '">'
						+			'<div class="popup-cropper__info">'
						+			'<div class="max-zoom"><i class="fa fa-compress" data-tooltip="Максимальное увеличение"></i></div>'
						+			'</div>'
						+			'<img id="imgCrop" src="' + self.templateItem.current.src + '">'
						+		'</div>'
						+		'<div class="popup-info">'
						+		name
						+		size
						+		dim
						+		'</div>'
						+		'<div class="popup-link">'
						+			'<span class="btn btn_big btn_green js-crop-rotate_l"><i class="fa fa-undo"></i></span>'
						+			'<span class="btn btn_big btn_green js-crop-rotate_r"><i class="fa fa-repeat"></i></span>'
						+			'<span class="btn btn_big btn_green js-crop-mirror"><i class="fa fa-arrows-h"></i></span>'
						+			'<span class="btn btn_big btn_green js-crop-submit">Принять</span>'
						+		'</div>';
					var $popup = self.popup.open('.popup_cropper', html);
					var $img = $popup.find('#imgCrop');

					$img.cropper({
						guides: false,
						aspectRatio: 100 / 125,
						dragMode: 'move',
						viewMode: 0,
						movable: true,
						cropBoxMovable: false,
						center: false,
						zoomable: true,
						restore: false,
						checkCrossOrigin: false,
						responsive: false,
						cropBoxResizable: false,
						toggleDragModeOnDblclick: false,
						minContainerHeight: 540,
						built: function(){

							$(this).cropper('setCropBoxData', {
								'width': 400,
								'height': 500,
								'top': 20,
								'left': 84
							});
						}
					}).on('zoom.cropper', function (e) {
						if (e.ratio > 1){
							$popup.addClass('mz');
							return false;
						} else {
							$popup.removeClass('mz');
						}
					});
				},

				getImage: function(){
					var $cropper = $('#imgCrop');
					var canvasBig = $cropper.cropper('getCroppedCanvas', { width: 600, height: 750 });
					var canvasSml = $cropper.cropper('getCroppedCanvas', { width: 172, height: 220 });
					var data = {};
					self.templateItem.current.data.img = canvasBig.toDataURL('image/jpeg', 0.9);
					self.templateItem.current.data.preview = canvasSml.toDataURL('image/jpeg', 0.9);

					$('[data-person-id=' + self.templateItem.current.id + ']')
						.addClass('loaded')
						.find('.upload-list')
						.append('<div class="upload-item"><div class="upload-item__remove"></div><img src="' + self.templateItem.current.data.preview + '"></div>');

					if ( !self.templateItem.storage[self.templateItem.current.id] ) self.templateItem.storage[self.templateItem.current.id] = [];

					for ( var key in self.templateItem.current.data ){
						if( self.templateItem.current.data.hasOwnProperty( key ) ) {
							data[key] = self.templateItem.current.data[key];
						}
					}
					self.templateItem.storage[self.templateItem.current.id].push(data);

					$('.popup_cropper .popup-close').click();
				},

				hovers: function(){
					$('body').on('mouseover', '[data-person-id]', function(){
						var $t = $(this);
						var id = $t.data('person-id');
						$('[data-area-id=' + id + ']').addClass('hover').trigger('mouseover');
					}).on('mouseleave', '[data-person-id]', function(){
						var $t = $(this);
						var id = $t.data('person-id');
						$('[data-area-id=' + id + ']').removeClass('hover').trigger('mouseleave');
					}).on('mouseover', '[data-area-id]', function(){
						var $t = $(this);
						var id = $t.data('area-id');
						$('[data-person-id=' + id + ']').addClass('hover');
					}).on('mouseleave', '[data-area-id]', function(){
						var $t = $(this);
						var id = $t.data('area-id');
						$('[data-person-id=' + id + ']').removeClass('hover');
					})
				},

				checkLength: function(){
					$('.upload-person').each(function(){
						var $t = $(this),
							$i = $t.find('.upload-list'),
							id = $t.data('person-id'),
							$area = $('[data-area-id='+id+']'),
							l = $i.find('.upload-item').size();

						if ( l > 0 ){
							$area.html('<i class="notes">'+l+'</i>');
						} else {
							$area.html('');
						}

					});
				},

				init: function(){
					self.templateItem.hovers();

					$('body').on('click', '.upload-add', function(){
						var $t = $(this),
							$wrap = $t.closest('.upload-person'),
							id = $wrap.data('person-id'),
							$face = $('[data-area-id=' + id + ']'),
							mes = {},
							faceTurn = $face.data('face-turn') ? $face.data('face-turn') : '';

						mes.top = $face.css('top').replace('px', '');
						mes.left = $face.css('left').replace('px', '');
						mes.width = $face.css('width').replace('px', '');
						mes.height = $face.css('height').replace('px', '');

						self.templateItem.current.ft = faceTurn;
						self.templateItem.current.id = id;
						self.templateItem.current.data.measure = mes;
						$('#templatePhotoUpload').click();
					}).on('click', '.area-item', function(){
						var $t = $(this),
							id = $t.data('area-id');
						$('[data-person-id=' + id + ']').find('.upload-add').click();
					}).on('change', '#templatePhotoUpload', function(e){
						self.preLoad.start('.item-view');
						if ( this.value && !/\.(jpg|jpeg|png|gif)$/i.test( this.value ) ) {
							self.popup.info('Вы выбрали не верный тип файла<br>Допустимые типы файлов: jpg, jpeg, png');
							self.preLoad.stop('.item-view');
						} else if (this.value && window.FileReader) {
							var file = e.target.files[0];
							if ( file ){
								var reader  = new FileReader();
								reader.readAsDataURL(file);
								reader.onloadend = function () {
									self.templateItem.current.src = reader.result;
									var img = new Image();
									img.src = self.templateItem.current.src;

									img.onload = function(){
										var width = +img.width;
										var height = +img.height;

										if ( width < 500 || height < 500 ){
											self.popup.info('Размеры картинки слишком маленькие,<br>ширина и высота должны быть<br>не&nbsp;менее&nbsp;500&nbsp;пикселей');
											self.preLoad.stop('.item-view');
										} else {
											self.templateItem.showImage(file.name, file.size, width, height);
											$('.js-template-submit').removeClass('btn_disable');
											self.preLoad.stop('.item-view');
										}
									};
								};
							}
						} else {
							self.preLoad.stop('.item-view');
						}
					}).on('click', '.popup_cropper .popup-close', function(){
						var $file = $('#templatePhotoUpload');
						self.templateItem.current.src = null;
						$file.val('');
					}).on('click', '.js-crop-submit', function(){
						self.templateItem.getImage();
						self.templateItem.checkLength();
					}).on('click', '.js-crop-rotate_l', function(){
						var $cropper = $('#imgCrop');
						$cropper.cropper('rotate', -90);
					}).on('click', '.js-crop-rotate_r', function(){
						var $cropper = $('#imgCrop');
						$cropper.cropper('rotate', 90);
					}).on('click', '.js-crop-mirror', function(){
						var $cropper = $('#imgCrop');
						$cropper.cropper('scaleX', -$cropper.cropper('getData').scaleX);
					}).on('click', '.upload-item__remove', function(){
						var $wrap = $(this).closest('.upload-item');
						var $allwrap = $wrap.closest('.upload-person');
						var ind = $wrap.index();
						self.templateItem.storage[self.templateItem.current.id].splice(ind,1);
						$wrap.remove();

						if ( !$allwrap.find('.upload-item').size() ){
							$allwrap.removeClass('loaded');
						}

						if ( !$('.upload-item').size() ){
							$('.js-template-submit').addClass('btn_disable');
						}

						self.templateItem.checkLength();

					}).on('click', '.js-template-submit', function(e){
						e.preventDefault();
						var $t = $(this);
						var data = self.templateItem.storage;
						data.templateId = $t.closest('.item').data('id');
						if ( !$t.hasClass('btn_disable')){
							self.preLoad.start('.item-view');
							$.ajax({
								url: '/ajax/upload_template_order',
								method: 'post',
								data: data,
								dataType: 'json'
							}).success(function (data) {
								//console.info(data);
								if ( data.status == 'success'){
									self.popup.info(data.message, function(){
										location.replace('/lk/orders');
									});
								} else if ((data.status == 'fail' || data.status == 'needauth') && data.message ){
									var footer = '<div class="popup-link"><span class="btn btn_big btn_green" data-popup=".popup_login">Войти</span>&nbsp;&nbsp;<span class="btn btn_big btn_green" data-popup=".popup_reg">Зарегистрироваться</span></div>';
									self.popup.info(data.message, false, footer);
								}
							}).complete(function (xhr, textStatus) {
								var res = JSON.parse(xhr.responseText);
								self.preLoad.stop('.item-view');
							});
						}
					});

				}
			},

			string: {
				declination: function (number, titles){
					cases = [2, 0, 1, 1, 1, 2];
					return titles[ (number%100>4 && number%100<20)? 2 : cases[(number%10<5)?number%10:5] ];
				}
			},

			checkForm: function(form){
				var $el = $(form),
					$sbmt = $el.find('[type=submit]'),
					wrong = false,
					mes = '';

				$el.find('[data-required]').each(function(){
					var $t = $(this),
						type = $t.data('required'),
						$wrap = $t.closest('.form-field'),
						val = $.trim($t.val()),
						rexp = '';
					$wrap.removeClass('success error');

					if ( $t.attr('type') == 'checkbox' && !$t.is(':checked') ){
						val = false;
					}

					switch (type) {
						case 'number':
							rexp = /^\d+$/i;
							break;
						case 'letter':
							rexp = /\w+$/i;
							break;
						case 'email':
							rexp = /^[-._a-z0-9]+@(?:[a-z0-9][-a-z0-9]+\.)+[a-z]{2,6}$/i;
							break;
						default:
							rexp = /.+$/igm;
					}

					if ( !rexp.test(val) || val == '0'){
						wrong = true;
						$wrap.addClass('error');
					} else {
						$wrap.addClass('success');
					}

				});


				return !!wrong;

			},

			browserDetect: {
				data: {
					template: 	'<div class="popup popup_browser">'
					+				'<div class="popup-inner">'
					+					'<div class="popup-layout">'
					+						'<div class="browser-detect__title"></div>'
					+						'<div class="browser-detect__subtitle"></div>'
					+						'<div class="browser-detect__message"></div>'
					+						'<div class="browser-detect__browsers">'
					+							'<a href="https://www.google.ru/chrome/browser/desktop/" class="browser-detect__item" target="_blank">Chrome</a> '
					+							'<a href="http://windows.microsoft.com/ru-ru/internet-explorer/download-ie" class="browser-detect__item" target="_blank">Internet Explorer</a> '
					+							'<a href="http://www.opera.com/" class="browser-detect__item" target="_blank">Opera</a> '
					+							'<a href="https://www.mozilla.org/ru/firefox/new/" class="browser-detect__item" target="_blank">Firefox</a> '
					+							( ( jQBrowser.platform == 'mac' || jQBrowser.platform == 'iphone' || jQBrowser.platform == 'ipad' )? '<a href="http://www.apple.com/ru/safari/" class="browser-detect__item" target="_blank">Safari</a> ' : '')
					+						'</div>'
					+				    '</div>'
					+				'</div>'
					+				'<div class="popup-overlay"></div>'
					+			'</div>',
					check: [
						['msie', '10', '', 'desktop'],
						['chrome', '42', 'win', 'desktop'],
						['firefox', '40'],
						['opera', '31'],
						['safari', '', 'win'],
						['safari', '8', 'mac'],
						['safari', '8', '', 'mobile']
					],
					onInit: function(){
						$('body').css('padding-right', self.viewPort.padding).addClass('overflow_hidden');
					}
//				, timeout: 9999
				},

				init: function(){
					$.browserDetect(self.browserDetect.data);
				}
			},

			tooltip: {
				init: function(){
					var $tooltip = $('.tooltip');

					if ( !$tooltip.size() ){
						$tooltip = $('<div/>', {'class': 'tooltip'}).appendTo('body');
					}

					$('body').on('mouseover', '[data-tooltip]', function(){
						var $t = $(this);
						var text = $t.attr('data-tooltip');
						$tooltip.html(text).stop(true, true).fadeIn(200);

						setTimeout(function(){
							var top = $t.offset().top - $tooltip.outerHeight();
							var left = $t.offset().left + ($t.outerWidth()/2);
							$tooltip.css({top: top, left: left});
						},0);

					}).on('mouseleave', '[data-tooltip]', function(){
						$tooltip.fadeOut(200);
					});
				}
			},

			fileUploader: {

				template: {
					templates: {
						box: '<div class="cards-list"></div>',
						item: '<div class="cards-item">\
							<div class="cards-item__frame">\
								{{fi-image}}\
							</div>\
							<div class="cards-item__legend">\
								<div class="cards-item__legend-name">{{fi-size2}}</div>\
								<div data-tooltip="{{fi-name}}" class="cards-item__legend-persons"><i class="fa fa-file-image-o"></i> {{fi-name}}</div>\
								<div class="cards-item__legend-favor"><i class="fa fa-trash-o js-remove-uploaded-file" data-tooltip="Удалить"></i></div>\
							</div>\
                        </div>',
						itemAppend: '<li class="jFiler-item">\
							<div class="jFiler-item-container">\
								<div class="jFiler-item-inner">\
									<div class="jFiler-item-thumb">\
										<div class="jFiler-item-status"></div>\
										<div class="jFiler-item-thumb-overlay">\
											<div class="jFiler-item-info">\
												<div style="display:table-cell;vertical-align: middle;">\
													<span class="jFiler-item-title"><b title="{{fi-name}}">{{fi-name}}</b></span>\
													<span class="jFiler-item-others">{{fi-size2}}</span>\
												</div>\
											</div>\
										</div>\
										{{fi-image}}\
									</div>\
									<div class="jFiler-item-assets jFiler-row">\
										<ul class="list-inline pull-left">\
											<li><span class="jFiler-item-others">{{fi-icon}}</span></li>\
										</ul>\
										<ul class="list-inline pull-right">\
											<li><a class="icon-jfi-trash jFiler-item-trash-action"></a></li>\
										</ul>\
									</div>\
								</div>\
							</div>\
						</li>',
						progressBar: '<div class="bar"></div>',
						itemAppendToEnd: true,
						removeConfirmation: true,
						_selectors: {
							list: '.cards-list',
							item: '.cards-item',
							progressBar: '.bar',
							remove: '.js-remove-uploaded-file'
						}
					},
					dragDrop: {}
				},

				init: function(){
					var $f = $('#PersonalStoryPhotos');

					$f.each(function(){
						var $t = $(this);

						$t.filer({
							showThumbs: true,
							extensions: ["jpg", "png", "gif"],
							changeInput: '<div class="jFiler-input-dragDrop"><div class="jFiler-input-inner"><div class="jFiler-input-icon"><i class="icon-jfi-folder"></i></div><a class="jFiler-input-choose-btn btn btn_big btn_green"><i class="fa fa-plus"></i>&nbsp;Добавить файлы</a></div></div>',

							addMore: true,
							templates: self.fileUploader.template.templates,
							dragDrop: self.fileUploader.template.dragDrop
							//uploadFile: self.fileUploader.template.uploadFile
						});
					});

				}
			},

			lk: {

				chat: function(){

					var $chatForm = $('.chat-form'), checked = false;

					$chatForm.on('submit', function(e){
						e.preventDefault();
						var $form = $(this);
						var $chatList = $form.find('.chat-list');
						var mes = $form.find('textarea');

						if ( self.checkForm($form) ) return false;

						$.ajax({
							url: $form.attr('action'),
							method: $form.attr('method'),
							data: $form.serialize(),
							dataType: 'json'
						}).success(function (data) {
							//console.info(data);
							if ( data.status == 'success'){
								var $mes = $('<div/>', {'class': 'chat-item', 'style': 'display:none'});
								$mes.append('<div class="chat-item__author">' + data.author + '</div>');
								$mes.append('<div class="chat-item__date">' + data.date + '</div>');
								$mes.append('<div class="chat-item__text">' + mes.val().replace(/\n/g, '<br>') + '</div>');
								$mes.appendTo($chatList);
								$mes.slideDown();
								mes.val('');
							}
						}).complete(function (xhr, textStatus) {
							//console.info(xhr);
						});

					}).on('focus', 'textarea', function(e){
						if ( checked ) return false;
						checked = true;

						var data = {}, $form = $(this).closest('form'), $chatList = $form.find('.chat-list');
						data.id_order = $form.find('[name=order-chat-id]').val();
						data.last_message_id = $form.find('[name=last_message_id]').val();

						$.ajax({
							type: "POST",
							url: '/lk/uncheck_new_messages_to_user',
							data: { id_order: data.id_order },
							dataType: "json",
							complete: function () {
								if (!!window.Worker){

									var chat = new Worker('/js/chat.js');
									chat.postMessage(data);
									chat.onmessage = function (e){
										console.info(e.data);
										var i = 0;
										for ( i; i < e.data.messages.length; i++ ){
											var $mes = $('<div/>', {'class': 'chat-item chat-item_sender', 'style': 'display:none'});
											$mes.append('<div class="chat-item__author">Администратор</div>');
											$mes.append('<div class="chat-item__date">' + e.data.messages[i].formed_dt + '</div>');
											$mes.append('<div class="chat-item__text">' + e.data.messages[i].message + '</div>');
											$mes.appendTo($chatList);
											$mes.slideDown();
										}

									};
								}
							}
						});



					});

				},

				message: function(){
					$('body').on('click', '[data-js-handle=message-to-unread]', function(e){
						e.preventDefault();
						var $t = $(this);
						var $mes = $t.closest('.message-item');
						var $listUnRead = $('.message-list_unread');

						$mes.slideUp(300, function(){
							if ($listUnRead.size()) $(this)
								.removeClass('message-item_viewed')
								.appendTo($listUnRead).slideDown(300)
								.find('.message-control__read')
								.toggleClass('fa-arrow-circle-up fa-check-circle')
								.attr({'data-js-handle': 'message-to-read' , 'data-tooltip': 'Отметить как прочитанное'});
						});

					}).on('click', '[data-js-handle=message-to-read]', function(e){
						e.preventDefault();
						var $t = $(this);
						var $mes = $t.closest('.message-item');
						var $listRead = $('.message-list_read');

						$mes.slideUp(300, function(){
							if ($listRead.size()) $(this)
								.addClass('message-item_viewed')
								.prependTo($listRead).slideDown(300)
								.find('.message-control__read')
								.toggleClass('fa-arrow-circle-up fa-check-circle')
								.attr({'data-js-handle': 'message-to-unread', 'data-tooltip': 'Отметить как непрочитанное'});
						});

					}).on('click', '.message-date, .message-theme', function(e){
						e.preventDefault();
						var $t = $(this);
						var $mes = $t.closest('.message-item');
						var html = $mes.find('.message-body').html();
						var $popup = self.popup.open('.popup_lk-message', html);
						$popup.attr('data-message-popup-id', $mes.data('message-id'));
					}).on('click', '[data-message-popup-id] .popup-close', function(e){
						e.preventDefault();
						var $t = $(this);
						var $popup = $t.closest('.popup');
						$('[data-message-id=' + $popup.attr('data-message-popup-id') + ']').find('[data-js-handle=message-to-read]').click();
					});
				},

				init: function(){

					if ( $('.message-item').size() ) self.lk.message();
					if ( $('.chat-form').size() ) self.lk.chat();

				}
			},

			viewPort: {

				padding: '',

				current: '',

				data: {
					'0': function(){
						self.viewPort.current = 0;
						//console.info('-----mobile-----');
					},
					'640': function(){
						self.viewPort.current = 1;
						//console.info('-----tabletVer-----');
					},
					'960': function(){
						self.viewPort.current = 2;
						//console.info('-----tabletHor-----');
					},
					'1200': function(){
						self.viewPort.current = 3;
						//console.info('-----desktop-----');
					},
					'1500': function(){
						self.viewPort.current = 4;
						//console.info('-----desktop HD-----');
					},
					'1800': function(){
						self.viewPort.current = 5;
						//console.info('-----full HD-----');
					}
				},

				init: function(){
					self.viewPort.padding = scrollBarWidth();
					var points = self.viewPort.data;
					if ( points ){
						points['Infinity'] = null;
						var sbw = scrollBarWidth(), curPoint = null;
						var ww = $(window).width() + sbw;
						checkCurrentViewport();
						$(window).on('resize', function(){
							ww = $(window).width() + sbw;
							checkCurrentViewport();
						});
					}

					function checkCurrentViewport(){
						var pp = 0, pc = null;
						$.each(points, function(point, callback){
							if ( point > ww ){
								if ( pp !== curPoint ) {
									curPoint = pp;
									pc();
								}
								return false;
							}
							pp = point; pc = callback;
						});
					}

					function scrollBarWidth(){
						var scrollDiv = document.createElement('div');
						scrollDiv.className = 'scroll_bar_measure';
						$(scrollDiv).css({
							width: '100px',
							height: '100px',
							overflow: 'scroll',
							position: 'absolute',
							top: '-9999px'
						});
						document.body.appendChild(scrollDiv);
						sbw = scrollDiv.offsetWidth - scrollDiv.clientWidth;
						document.body.removeChild(scrollDiv);
						return sbw;
					}

				}
			},

			preLoad: {

				start: function(el){
					var $el = $(el);
					//$('body').addClass('loading');
					$el.addClass('loading preloader-wrap').append('<div class="preloader"><div class="preloader-i"></div></div>');
				},

				stop: function(el){
					var $el = $(el);
					//$('body').removeClass('loading');
					$el.removeClass('loading preloader-wrap').find('.preloader').remove();
				},

				init: function(){
					$('body').on('click', function(e){
						if ( $(this).hasClass('loading')){
							e.preventDefault();
							e.stopPropagation();
							return false;
						}
					}).on('click', '.preloader', function(e){
						e.preventDefault();
						e.stopPropagation();
						return false;
					})
				}

			},

			popup: {
				create: function(popup){
					var pref = popup.indexOf('#') == 0  ? 'id' : 'class';
					var name = popup.replace(/^[\.#]/,'');
					var $popup = $('<div class="popup">'
						+			'<div class="popup-inner">'
						+				'<div class="popup-layout">'
						+					'<div class="popup-close"></div>'
						+					'<div class="popup-content"></div>'
						+				'</div>'
						+			'</div>'
						+			'<div class="popup-overlay"></div>'
						+		'</div>').appendTo('body');

					if ( pref == 'id'){
						$popup.attr(pref, name);
					} else {
						$popup.addClass(name);
					}

					return $popup;
				},

				open: function(popup, html){
					self.popup.close('.popup');
					var $popup = $(popup);
					if (!$popup.size()){
						$popup = self.popup.create(popup);
					}
					if( html ){
						$popup.find('.popup-content').html(html);
					}
					$('body').css('padding-right', self.viewPort.padding).addClass('overflow_hidden');
					return $popup.show();
				},

				close: function(popup){
					var $popup = $(popup);
					$('body').css('padding-right', 0).removeClass('overflow_hidden');
					$popup.hide();
				},

				info: function(mes, callback, footer){
					var html = '<div class="popup-text">' + mes + '</div>'
						+	(footer ? footer : '<div class="popup-link"><span class="btn btn_big btn_green js-popup-close">Ок</span></div>');
					self.popup.open('.popup_info', html);

					if ( callback ) {
						$('.popup_info').find('.btn').click(callback);
					}
				},

				remove: function(popup){
					var $popup = $(popup);
					$('body').css('padding-right', 0).removeClass('overflow_hidden');
					$popup.remove();
				}
			},

			headerMenu: {

				check:function(){

					var $header = $('.header');

					if ( $header.offset().top >= $(window).scrollTop() ){
						$header.removeClass('fixed');
					} else {
						$header.addClass('fixed');
					}

				},

				init: function(){
					self.headerMenu.check();
					$(window).on('scroll', function(){
						self.headerMenu.check();
					});
				}

			},

			parallax: {

				init: function(){

					if ( !Modernizr.touch ){
						$('.parallax').each(function(){
							var parallax = new Parallax(this);
						});
					}
				}
			},

			welcomeSlider: {

				timer: null,
				num: 0,
				current: 0,

				slider: function(){

					var $slider = $('.header-welcome__slider');
					var $slide = $slider.find('.header-welcome__slider-item');
					self.welcomeSlider.num = $slide.length;
					self.welcomeSlider.loadSlide();
					self.welcomeSlider.timer = setInterval(function(){
						self.welcomeSlider.current = ( self.welcomeSlider.current + 1 ) % self.welcomeSlider.num;
						self.welcomeSlider.loadSlide();
					}, 5000);

				},

				loadSlide: function(){
					var $slider = $('.header-welcome__slider');
					var $slide = $slider.find('.header-welcome__slider-item');
					var $el = $slide.eq(self.welcomeSlider.current);

					$slide.filter('.active').removeClass('active');

					$el.find('[data-src]').each(function(){
						var $t = $(this);
						var src = $t.data('src');
						//self.preLoad.start($slider);

						var $img = $('<img src="' + src + '"/>');

						$img.load(function(){
							$el.addClass('active');
							$t.attr('src', src);
							//setTimeout(function(){
							//	self.preLoad.stop($slider);
							//}, 100);
						});


					});
				},

				init: function(){

					var $hw = $('.header-welcome');

					if ( $hw.size() ) self.welcomeSlider.slider();

					$('body')
						.on('click', '.header-welcome__control-prev', function(e){
							e.preventDefault();
							e.stopPropagation();
							self.welcomeSlider.current = ( self.welcomeSlider.current > 0 ) ? ( self.welcomeSlider.current - 1 ) : self.welcomeSlider.num - 1 ;
							self.welcomeSlider.loadSlide();
						})
						.on('click', '.header-welcome__control-next', function(e){
							e.preventDefault();
							e.stopPropagation();
							self.welcomeSlider.current = ( self.welcomeSlider.current + 1 ) % self.welcomeSlider.num;
							self.welcomeSlider.loadSlide();
						})
						.on('click', '.header-welcome__control-close', function(e){
							e.preventDefault();
							e.stopPropagation();
							$hw.toggleClass('zoom nozoom');
							self.welcomeSlider.slider();
						})
						.on('click', '.header-welcome__slider', function(e){
							if ( $hw.hasClass('nozoom')){
								$hw.toggleClass('zoom nozoom');
								clearInterval(self.welcomeSlider.timer);
							} else if ( $hw.hasClass('zoom') ){
								self.welcomeSlider.current = ( self.welcomeSlider.current + 1 ) % self.welcomeSlider.num;
								self.welcomeSlider.loadSlide();
							}

						})
						.on('click', function(e){
							if ( $hw.hasClass('zoom') && !$(e.target).closest('.header-welcome__slider').length ) {
								$hw.toggleClass('zoom nozoom');
								self.welcomeSlider.slider();
							}
						})
						.on('click', '.js-show-example', function(){
							setTimeout(function(){
								$('.header-welcome__slider').click();
								$('html,body').animate({scrollTop:0}, 300);
							}, 0);
						})
					;


				}
			},

			user: {
				setAuth: function(){
					var $userBox = $('.nav-list_user'), html;

					html = '<li class="nav-item"><a href="/lk/orders" title="Вход в личный кабинет" class="nav-link">Личный кабинет</a></li><li class="nav-item"><a href="/lk/logout" title="Выйти" class="nav-link"><i class="fa fa-sign-out"></i><span class="nav-item__full">Выйти</span></a></li>';

					$userBox.html(html);
				}
			}

		},
		_public: {
			viewPort: function(){ return self.viewPort.current;},
			popup: {
				open: function(popup, html){ return self.popup.open(popup, html);},
				close: function(popup){ return self.popup.close(popup);},
				remove: function(popup){ return self.popup.remove(popup);}
			}

		},
		init: function() {

			self.browserDetect.init();
			self.viewPort.init();
			self.preLoad.init();
			self.tooltip.init();
			self.templateItem.init();
			self.headerMenu.init();
			self.lk.init();
			self.welcomeSlider.init();
			self.parallax.init();
			self.fileUploader.init();


			if ( $('.cards-list_slider').size() ) {
				$('.cards-list_slider').slick({
					dots: false,
					infinite: true,
					speed: 300,
					slidesToShow: 6,
					slidesToScroll: 1,
					swipeToSlide: true,
					touchMove: false,
					responsive: [
						{
							breakpoint: 1920,
							settings: {
								slidesToShow: 5
							}
						},
						{
							breakpoint: 1600,
							settings: {
								slidesToShow: 4
							}
						},
						{
							breakpoint: 1280,
							settings: {
								slidesToShow: 3
							}
						},
						{
							breakpoint: 960,
							settings: {
								slidesToShow: 2
							}
						},
						{
							breakpoint: 640,
							settings: {
								slidesToShow: 1
							}
						}
					]
				});
			}


			$('[data-required=phone]').mask('+7 (999) 999-99-99');


			$('body').on('click', '.popup-close, .js-popup-close', function(e){
				e.preventDefault();
				e.stopPropagation();
				self.popup.close($(this).closest('.popup'));
			}).on('click', '[data-popup]', function(e){
				e.preventDefault();
				e.stopPropagation();
				self.popup.close('.popup');
				self.popup.open($(this).data('popup'));
			}).on('submit', '.form_js', function(e){
				e.preventDefault();
				e.stopPropagation();
				var $form = $(this), type = $form.data('form-type'), formData;

				if ( self.checkForm($form) ) return false;

				$form.removeClass('form_error');

				var prop = {
					url: $form.attr('action'),
					type: $form.attr('method')
				};

				if ( $form.attr('enctype') && $form.attr('enctype').indexOf('form-data') >= 0 ){
					formData = new FormData($(this)[0]);
					prop.data = formData;
					prop.async = false;
					prop.cache = false;
					prop.contentType = false;
					prop.processData = false;
				} else {
					formData = $form.serialize();
					prop.data = $form.serialize();
					prop.dataType = 'json';
				}

				prop.data = formData;

				$.ajax(prop).success(function (data) {

				}).complete(function (xhr, textStatus) {
					var res = JSON.parse(xhr.responseText);
					if ( res.status == 'success' ) {

						switch (type){
							case 'auth':
								if ( Object.keys(self.templateItem.storage).length > 0 || $('[data-form-type=personalstory]').size() ){
									self.user.setAuth();
									self.popup.info('Вы успешно зарегистрировались');
								} else {
									location.reload();
								}
								break;
							case 'reg':
								if ( Object.keys(self.templateItem.storage).length > 0 || $('[data-form-type=personalstory]').size() ){
									self.user.setAuth();
									self.popup.info('Вы успешно зарегистрировались');
								} else {
									location.reload();
								}
								break;
							case 'feedback':
								self.popup.info('Спасибо!<br>Скоро мы ответим на ваше сообщение.', function(){
									window.location = location.origin;
								});
								break;
							default:
								location.reload();
						}
					}
					else if ( res.status == 'needauth' ) {
						var footer = '<div class="popup-link"><span class="btn btn_big btn_green" data-popup=".popup_login">Войти</span>&nbsp;&nbsp;<span class="btn btn_big btn_green" data-popup=".popup_reg">Зарегистрироваться</span></div>';
						self.popup.info(res.message, false, footer);
					}
					else if ( res.message ) {
						if ( $form.find('.form-message').size() ) {
							$form.find('.form-message').html(res.message);
						} else {
							self.popup.info(res.message);
						}
					}
				});


			}).on('click', '.js-favorite', function(e){
				e.preventDefault();
				var $t = $(this);
				var data = {};
				data.action = $t.hasClass('fa-star') ? 'remove-from-favorite' : 'add-to-favorite';
				data.id = $t.data('id');

				$.ajax({
					url: '/ajax/favorite',
					method: 'post',
					data: data,
					dataType: 'json'
				}).success(function (data) {
					//console.info(data);
					if ( data.status == 'success'){
						var tooltip = $t.hasClass('fa-star') ? 'Добавить в&nbsp;избранное' : 'Удалить из&nbsp;избранного';
						$t.toggleClass('fa-star fa-star-o').attr('data-tooltip', tooltip);
						$t.trigger('mouseleave');
					} else if (data.message) {
						self.popup.info(data.message);
					}
				}).complete(function (xhr, textStatus) {
					//console.info(xhr);
				});

			}).on('click', '.js-order-cancel', function(e){
				e.preventDefault();
				var $t = $(this);

				$.ajax({
					url: '/ajax/ordercancel',
					method: 'post',
					data: {'order-id': $t.data('order-id')},
					dataType: 'json'
				}).success(function (data) {
					if ( data.status == 'success'){
						self.popup.info('Заказ № ' + $t.data('order-id') + ' отменен.');
						$t.closest('.lk-table__row').remove();
					} else if (data.message) {
						self.popup.info(data.message);
					}
				}).complete(function (xhr, textStatus) {
					//console.info(xhr);
				});

			}).on('click', '.cards-item__frame', function(e){

				if ( $(this).closest('.jFiler').size() ) return false;

				var $item = $(this).closest('.cards-item'),
					$items = $item.parent().find('.cards-item'),
					$img = $('<img/>', {
						src: $item.data('src')
					});

				self.preLoad.start($item);

				$img.load(function(){

					var name = $item.data('name') ? $item.data('name') : '',
						src = $item.data('src') ? $item.data('src') : '',
						href = $item.data('href') ? $item.data('href') : '#',
						arrowPrev = $item.prev().attr('data-id') ? '<div class="arrow arrow_prev" data-slide-id="' + $item.prev().attr('data-id') + '"><</div>' : '',
						arrowNext = $item.next().attr('data-id') ? '<div class="arrow arrow_next" data-slide-id="' + $item.next().attr('data-id') + '">></div></div>' : '',

						arrows = '<div class="popup_preview__arrows">' + arrowPrev + arrowNext;

					var person = $item.data('person') ? ($item.data('person') == 1 ? '1 персонаж' : ('до ' + $item.data('person') + ' ' + self.string.declination($item.data('person'), ['персонажа', 'персонажей', 'персонажей']))) : '';

					var html = '<div class="popup_preview__img"><img src="' + src + '"></div>'
						+		'<div class="popup_preview__name">' + name + '<br><span>\(' + person + '\)</span></div>'
						+		($items.length > 1 ? arrows : '')
						+		'<div class="popup_preview__link"><a href="' + href + '" class="btn btn_big btn_green">Выбрать</a></div>';
					self.popup.open('.popup_preview', html);
					self.preLoad.stop($item);
				});

			}).on('click', '.popup_preview__arrows .arrow', function(){
				var $t = $(this);

				$('.cards-item[data-id=' + $t.data('slide-id') + ']').not('.slick-cloned').find('.cards-item__frame').click();

				if ( $t.hasClass('arrow_prev') ){
					$('.slick-prev').click();
				} else if ( $t.hasClass('arrow_next')){
					$('.slick-next').click();
				}

			}).on('click', '.nav-handler', function(){
				$('body').toggleClass('open-nav');
			});

		}
	};
	var self = {};
	var loader = function(){
		self = app.module;
		//jQuery.app = app._public;
		jQuery.app = app.module;
		app.init();
	};
	var ali = setInterval(function(){
		if (typeof jQuery !== 'function') return;
		clearInterval(ali);
		setTimeout(loader, 0);
	}, 50);

})();

















